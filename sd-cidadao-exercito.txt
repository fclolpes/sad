UF,CURSO,EFETIVO_(PESSOAS),RECURSOS_(R$)
AL,ELETRICISTA DE AUTOM�VEIS,2,1.558.80                                              
AL,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,2,2.230.80                         
AL,MOTORISTA DE TRANSPORTE DE EMERG�NCIA,2,510.00                                    
AL,MOTORISTA DE TRANSPORTE DE PASSAGEIROS,2,510.00                                   
AL,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,2,510.00                            
AM,ALMOXARIFE DE OBRAS,7,0.00                                                        
AM,AUXILIAR DE AGROPECU�RIA,19,0.00                                                  
AM,BOAS PR�TICAS NA MANIPULA��O DE ALIMENTOS,25,0.00                                 
AM,CISCO CCNA 1,5,0.00                                                               
AM,CONFEITEIRO,20,0.00                                                               
AM,COZINHEIRO INDUSTRIAL,42,0.00                                                     
AM,DESENHISTA DA CONSTRU��O CIVIL,2,0.00                                             
AM,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,69,23.206.40                       
AM,GAR�OM,37,0.00                                                                    
AM,INFORM�TICA APLICADA A CONSTRU��O CIVIL,2,0.00                                    
AM,INSTALADOR E REPARADOR DE REDES DE COMPUTADORES,25,12.000.00                      
AM,INSTALADOR HIDR�ULICO RESIDENCIAL,27,10.419.20                                    
AM,MEC�NICO DE MOTORES DE POPA,12,0.00                                               
AM,MEC�NICO DE REFRIGERA��O E CLIMATIZA��O RESIDENCIAL,31,12.400.00                  
AM,MONTADOR DE ANDAIMES,3,0.00                                                       
AM,MONTADOR DE SISTEMAS DE CONSTRU��O A SECO,6,0.00                                  
AM,MONTADOR E REPARADOR DE COMPUTADORES,43,12.000.00                                 
AM,MOTORISTA DE TRANSPORTE DE EMERG�NCIA,23,0.00                                     
AM,MUDAN�A DE CATEGORIA CNH,53,0.00                                                  
AM,NR 10 - SISTEMA EL�TRICO DE POT�NCIA (SEP),54,0.00                                
AM,PEDREIRO DE ALVENARIA,11,0.00                                                     
AM,PINTOR DE OBRAS IMOBILI�RIAS,23,0.00                                              
AM,SOLDADOR DE ESTRUTURAS E TUBULA��O NO PROCESSO MIG/MAG,6,0.00                     
AM,VIGILANTE,26,24.076.00                                                            
AP,CONFEITEIRO,9,4.219.20                                                            
AP,ELETRICISTA DE AUTOM�VEIS,8,2.803.20                                              
AP,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,8,4.134.40                         
AP,SALGADEIRO,8,3.520.00                                                             
AP,SOLDADOR NO PROCESSO ELETRODO REVESTIDO EM A�O CARBONO E A�O BAIXA LIGA,8,5.318.40
BA,AUXILIAR DE COZINHA,20,11.424.00                                                  
BA,INSTALADOR E REPARADOR DE REDES - CABOS E EQUIPAMENTOS TELEF�NICOS,20,12.806.40   
BA,MUDAN�A DE CATEGORIA CNH,72,25.200.00                                             
CE,MONTADOR E REPARADOR DE COMPUTADORES,20,22.048.00                                 
DF,CONFEITEIRO,21,27.669.60                                                          
DF,CURSO DRYWALL COM ACABAMENTO EM PINTURA RESIDENCIAL,16,18.892.80                  
DF,EDITOR GR�FICO,21,22.806.00                                                       
DF,ELETRICISTA DE AUTOM�VEIS,18,0.00                                                 
DF,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,21,22.579.20                       
DF,ENCANADOR INSTALADOR PREDIAL,21,16.228.80                                         
DF,GAR�OM,20,23.650.00                                                               
DF,INSTALADOR DE SISTEMAS ELETR�NICOS DE SEGURAN�A,21,18.244.80                      
DF,INSTALADOR E REPARADOR DE FIBRAS �TICAS,23,19.982.40                              
DF,MARCENEIRO,20,21.920.00                                                           
DF,MEC�NICO DE INJE��O ELETR�NICA FLEX E DIESEL,24,0.00                              
DF,MEC�NICO DE REFRIGERA��O E CLIMATIZA��O RESIDENCIAL,21,19.084.80                  
DF,MOTORISTA DE TRANSPORTE DE EMERG�NCIA,25,5.000.00                                 
DF,MOTORISTA DE TRANSPORTE DE PASSAGEIROS,68,9.400.00                                
DF,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,25,5.000.00                         
DF,PADEIRO,20,19.360.00                                                              
DF,PEDREIRO,20,17.536.00                                                             
DF,PINTOR DE OBRAS IMOBILI�RIAS,20,17.536.00                                         
DF,SEGURAN�A NA MANIPULA��O DE ALIMENTOS,20,0.00                                     
DF,T�CNICO DE PRODU��O DE P�ES CASEIROS E ARTESANAIS,20,6.336.00                     
DF,VIGILANTE,213,148.674.00                                                          
DF,WEB DESIGNER,22,15.206.40                                                         
ES,BOMBEIRO CIVIL,20,0.00                                                            
ES,MEC�NICO DE MOTORES A DIESEL,20,16.000.00                                         
ES,OPERADOR DE EMPILHADEIRA,20,16.000.00                                             
GO,CONTROLE AUTOMATIZADO DE PASSAGEIROS - CAP,1,0.00                                 
GO,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,47,0.00                            
GO,TORNEIRO MEC�NICO,1,0.00                                                          
GO,VIGILANTE,26,15.974.40                                                            
MA,APRENDER A EMPREENDER,20,0.00                                                     
MA,B�SICO DE DESENHO ARQUITET�NICO,20,0.00                                           
MA,BOAS PR�TICAS NA MANIPULA��O DE ALIMENTOS,3,24.00                                 
MA,EDITOR DE PROJETO VISUAL GR�FICO,2,20.00                                          
MA,EDUCA��O AMBIENTAL,20,0.00                                                        
MA,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,2,720.00                           
MA,EXCEL B�SICO,2,30.00                                                              
MA,FINAN�AS PESSOAIS,20,0.00                                                         
MA,FOT�GRAFO,2,20.00                                                                 
MA,FUNDAMENTOS DE LOG�STICA,20,0.00                                                  
MA,L�GICA DE PROGRAMA��O,20,0.00                                                     
MA,MARCENEIRO,2,640.00                                                               
MA,METROLOGIA,20,0.00                                                                
MA,NO��ES B�SICAS DE MEC�NICA AUTOMOTIVA,20,0.00                                     
MA,OPERADOR DE PROCESSOS DE PRODU��O DE CARNES E DERIVADOS,2,16.00                   
MA,PEDREIRO DE ALVENARIA,5,2.368.00                                                  
MA,SEGURAN�A DO TRABALHO,20,0.00                                                     
MA,TECNOLOGIA DA INFORMA��O E COMUNICA��O,20,0.00                                    
MS,AUXILIAR DE COZINHA,20,22.000.00                                                  
MS,CONTROLE DE FORMIGAS CORTADEIRAS,27,0.00                                          
MS,COZINHEIRO,10,30.250.00                                                           
MS,CURSO DE EXTENS�O EM SOLDAGEM,40,0.00                                             
MS,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,15,17.550.00                       
MS,FLORICULTURA - JARDINAGEM E PAISAGISMO,13,0.00                                    
MS,GAR�OM,15,20.625.00                                                               
MS,IMPLANTA��O E MANEJO B�SICO DE HORTA,17,0.00                                      
MS,INSTALADOR HIDR�ULICO RESIDENCIAL,15,15.600.00                                    
MS,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,2,0.00                              
MS,OPERADOR DE COMPUTADOR,25,0.00                                                    
MS,OPERADOR DE MOTOSSERRA,12,0.00                                                    
MS,PADEIRO,37,16.500.00                                                              
MS,PIZZAIOLO,20,17.600.00                                                            
MS,PLANEJAMENTO - INSTALA��O - MANUTEN��O E CONFIGURA��O DE REDES DE COMPUTADORES,20,
MS,PLANTIO E MANEJO DE POMAR - CULTIVO DE MARACUJ�,14,0.00                           
MS,PLANTIO E MANEJO DE POMAR - CULTIVO DE UVA,15,0.00                                
MS,PROCESSAMENTO CASEIRO DO TOMATE,12,0.00                                           
MS,PRODU��O DE ALIMENTOS SAUD�VEIS,12,0.00                                           
MS,PRODUTOR DE DERIVADOS DO LEITE,12,0.00                                            
MS,PRODUTOR DE MANDIOCA,14,0.00                                                      
MS,PROGRAMADOR WEB,20,0.00                                                           
MS,T�CNICAS DE FINGER FOOD,20,0.00                                                   
PA,APLICADOR DE REVESTIMENTO CER�MICO,1,220.00                                       
PA,AUXILIAR DE SA�DE BUCAL,15,0.00                                                   
PA,CUMIM,25,16.000.00                                                                
PA,ELETRICISTA DE AUTOM�VEIS,20,12.224.00                                            
PA,GAR�OM,25,24.000.00                                                               
PA,INSTALADOR HIDR�ULICO RESIDENCIAL,2,440.00                                        
PA,MASSEIRO,5,0.00                                                                   
PA,MEC�NICO DE AUTOM�VEL,5,4.400.00                                                  
PA,MEC�NICO DE MOTORES DE POPA,5,4.400.00                                            
PA,MEC�NICO DE REFRIGERA��O E CLIMATIZA��O RESIDENCIAL,20,12.224.00                  
PA,MONTADOR E REPARADOR DE COMPUTADORES,25,16.500.00                                 
PA,MOTORISTA DE TRANSPORTE DE CARGA,11,4.400.00                                      
PA,MUDAN�A DE CATEGORIA CNH,39,28.080.00                                             
PA,OPERA��O DE EMPILHADEIRA,6,0.00                                                   
PA,OPERADOR DE EQUIPAMENTOS DE MINA,22,8.800.00                                      
PA,PADEIRO,6,0.00                                                                    
PA,SOLDADOR DE ESTRUTURAS E TUBULA��O NO PROCESSO MIG/MAG,6,0.00                     
PA,T�CNICAS DE MANUTEN��O DO SISTEMA DE INJE��O,3,990.00                             
PA,T�CNICAS DE RECUPERA��O EM SISTEMAS DE INJE��O,6,0.00                             
PB,MOTORISTA DE TRANSPORTE DE PASSAGEIROS,18,3.600.00                                
PB,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,18,3.600.00                         
PI,MONTADOR E REPARADOR DE COMPUTADORES,6,5.040.00                                   
RJ,ALIMENTADOR DE LINHA DE PRODU��O,4,2.432.00                                       
RJ,ASSISTENTE DE PLANEJAMENTO - PROGRAMA��O E CONTROLE DE PRODU��O,2,864.00          
RJ,AUXILIAR DE SA�DE BUCAL,62,10.350.00                                              
RJ,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,42,14.088.00                       
RJ,ENCANADOR INSTALADOR PREDIAL,10,0.00                                              
RJ,INSTALADOR E REPARADOR DE FIBRAS �TICAS,2,1.312.00                                
RJ,INSTALADOR HIDR�ULICO RESIDENCIAL,20,0.00                                         
RJ,MEC�NICO DE AR CONDICIONADO AUTOMOTIVO,1,412.80                                   
RJ,MEC�NICO DE REFRIGERA��O E CLIMATIZA��O RESIDENCIAL,10,7.140.00                   
RJ,MOTORISTA DE TRANSPORTE DE CARGA,2,600.00                                         
RJ,MOTORISTA DE TRANSPORTE DE EMERG�NCIA,24,5.520.00                                 
RJ,MOTORISTA DE TRANSPORTE DE PASSAGEIROS,20,4.500.00                                
RJ,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,20,4.500.00                         
RJ,PEDREIRO DE ALVENARIA ESTRUTURAL,50,23.400.00                                     
RJ,PEDREIRO DE REVESTIMENTO EM ARGAMASSA,23,0.00                                     
RJ,PINTOR DE OBRAS IMOBILI�RIAS,33,0.00                                              
RJ,SERRALHEIRO DE MATERIAIS FERROSOS,3,6.824.52                                      
RN,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,16,9.369.60                        
RN,ENCANADOR HIDR�ULICO,16,7.782.40                                                  
RN,PEDREIRO DE ALVENARIA,16,8.780.80                                                 
RO,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,25,18.450.00                       
RS,AUXILIAR DE COZINHA,45,40.968.00                                                  
RS,CABEAMENTO E REDES ELETR�NICAS DE DADOS,24,6.000.00                               
RS,EDITOR DE V�DEO,20,18.784.00                                                      
RS,FRENTISTA,6,540.00                                                                
RS,GEST�O DE FROTA,12,1.080.00                                                       
RS,L�GICA DE PROGRAMA��O,9,0.00                                                      
RS,MEC�NICO DE MOTORES A DIESEL,10,2.000.00                                          
RS,MOTORISTA DE TRANSPORTE DE CARGA,174,35.708.75                                    
RS,MOTORISTA DE TRANSPORTE DE EMERG�NCIA,248,51.480.00                               
RS,MOTORISTA DE TRANSPORTE DE PASSAGEIROS,280,57.427.50                              
RS,MOTORISTA DE TRANSPORTE DE PRODUTOS PERIGOSOS,263,55.685.00                       
RS,MOTORISTA DE TRANSPORTE ESCOLAR,125,30.250.00                                     
RS,MUDAN�A DE CATEGORIA CNH,377,131.950.00                                           
RS,PIZZAIOLO,20,21.280.00                                                            
RS,VIGILANTE,10,0.00                                                                 
SP,AJUSTADOR MEC�NICO,1,0.00                                                         
SP,AUTOMA��O HIDR�ULICA INDUSTRIAL,2,0.00                                            
SP,AUXILIAR DE MEC�NICO DE AUTOM�VEIS,3,0.00                                         
SP,COMANDOS EL�TRICOS,1,0.00                                                         
SP,CONFEITEIRO,1,0.00                                                                
SP,COSTUREIRO,4,0.00                                                                 
SP,EDITOR DE PROJETO VISUAL GR�FICO,2,0.00                                           
SP,ELETRICISTA DE AUTOM�VEIS,5,0.00                                                  
SP,ELETRICISTA INSTALADOR PREDIAL DE BAIXA TENS�O,6,0.00                             
SP,IMPERMEABILIZA��O DE ESTRUTURAS,2,0.00                                            
SP,IMPLEMENTA��O DE REDES LOCAIS,2,0.00                                              
SP,INSTALADOR HIDR�ULICO RESIDENCIAL,3,0.00                                          
SP,MEC�NICO DE FREIOS - SUSPENS�O E DIRE��O DE VE�CULOS LEVES,4,0.00                 
SP,MEC�NICO DE MOTORES CICLO OTTO,2,0.00                                             
SP,MONTADOR E REPARADOR DE COMPUTADORES,2,0.00                                       
SP,MUDAN�A DE CATEGORIA CNH,20,0.00                                                  
SP,OPERADOR DE EMPILHADEIRA,3,0.00                                                   
SP,PADEIRO,7,0.00                                                                    
SP,PEDREIRO,3,0.00                                                                   
SP,PEDREIRO DE REVESTIMENTO EM ARGAMASSA,2,0.00                                      
SP,PINTOR DE OBRAS IMOBILI�RIAS,9,0.00                                               
SP,SALGADEIRO,2,0.00                                                                 
SP,SOLIDWORKS,4,0.00                                                                 
TO,AGENTE DE RECEP��O E RESERVAS EM MEIOS DE HOSPEDAGEM,12,5.760.00                  
TO,BALCONISTA DE FARM�CIA,8,5.606.40                                                 
TO,FOT�GRAFO,6,4.503.00                                                              
TO,INSTALADOR E REPARADOR DE REDES DE COMPUTADORES,8,4.800.00                        
TO,MONTADOR E REPARADOR DE COMPUTADORES,16,7.193.60                                  
TO,OPERADOR DE COMPUTADOR,10,5.488.00                                                
TOTAL,,4.679 ,1.595.687.37                                                           
